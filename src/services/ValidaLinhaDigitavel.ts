import moment from "moment"

class ValidarLinhaDigitavel { 

    public async execute(barCode: string){
        if(barCode.length == 44){
            const titulos = await this.titulos(barCode)
            return titulos
        } else if(barCode.length == 47){
            const convenios = await this.convenios(barCode)
            return convenios
        } else {
            throw new Error(`A linha digitável informada é invalida pois contém ${barCode.length} dígitos. Deve conter 44 ou 47 dígitos`)
        }       
    }

    public async titulos(barCode: string){   

        const codBanco = barCode.substring(0,3)
        const codMoeda = barCode.substring(3,4) 
        const dvCodBarra = barCode.substring(4,5)
        const fatorVenc = barCode.substring(5,9)        
        const convenio = barCode.substring(19,23)
        const complemento = barCode.substring(23,30)
        const agencia = barCode.substring(30,34)
        const conta = barCode.substring(34,42)
        const cart = barCode.substring(42,44)

        const expirationDate = await this.obterVencimento(parseInt('1000')) 
        const amount = Math.round(parseInt(barCode.substring(9,19)))/100   
        
        console.log(barCode.length)
        
        const restult = {
            barCode,
            amount,
            expirationDate            
        }
        
        return restult
    }

    public async convenios(barCode: string){

        const codBanco = barCode.substring(0,3)
        const codMoeda = barCode.substring(3,4) 
        const dvCodBarra = barCode.substring(4,5)
        const fatorVenc = barCode.substring(5,9)        
        const convenio = barCode.substring(19,23)
        const complemento = barCode.substring(23,30)
        const agencia = barCode.substring(30,34)
        const conta = barCode.substring(34,42)
        const cart = barCode.substring(42,44)

        const expirationDate = await this.obterVencimento(parseInt('1000')) 
        const amount = Math.round(parseInt(barCode.substring(9,19)))/100   
        
        console.log(barCode.length)
        
        const restult = {
            barCode,
            amount,
            expirationDate            
        }       

        return restult
    }

    public async obterVencimento(fator: number){
      var dataBase = new Date(1997,10-1,7) 
      var venciemnto = moment(dataBase.setDate(dataBase.getDate() + fator)).format('YYYY-MM-DD') 

      return venciemnto

    }
}

export default ValidarLinhaDigitavel