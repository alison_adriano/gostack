import { Router } from "express";
import boletosRouter from "./boletos.routes";

const routes = Router();

routes.use('/boletos', boletosRouter)

export default routes
