import {  Router } from "express";
import ValidarLinhaDigitavel from "../services/ValidaLinhaDigitavel";

const boletosRouter = Router();

boletosRouter.get('/:barCode', async (request, response) => {
  const { barCode } = request.params

  try{
    const validaLinhaDigitavel = new ValidarLinhaDigitavel()
    const result = await validaLinhaDigitavel.execute(barCode)
    return response.status(200).json({status: 200, result })
  } catch (error: any) {
    return response.status(400).json({status: 400, mensagem: error.message})
  }

})

export default boletosRouter
